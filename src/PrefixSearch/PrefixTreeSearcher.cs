﻿using System.Collections.Generic;
using System.Linq;

namespace PrefixSearch
{
    public partial class Program
    {
        public class PrefixTreeSearcher
        {
            public IEnumerable<string> GetCharacterCombinationsForWord(PrefixTreeNode rootNode, string word)
            {
                var currentNode = rootNode;
                var combinations = new List<string>();

                if (string.IsNullOrEmpty(word))
                {
                    return combinations;
                }

                for (int charIndex = 0; charIndex < word.Length; ++charIndex)
                {
                    var character = word[charIndex];
                    if (!currentNode.Children.ContainsKey(character))
                    {
                        return combinations;
                    }

                    currentNode = currentNode.Children[character];
                    if (currentNode.IsEndOfWord)
                    {
                        string wordSuffix = word.Substring(charIndex + 1, word.Length - 1 - charIndex);
                        var nextList = GetCharacterCombinationsForWord(rootNode, wordSuffix);
                        if (nextList.Any())
                        {
                            var prefix = word.Substring(0, charIndex + 1);
                            combinations.AddRange(nextList.Select(item => $"{prefix}+{item}"));
                        }
                    }
                }

                if (currentNode.IsEndOfWord)
                {
                    combinations.Add(word);
                }

                return combinations;
            }
        }
    }
}
