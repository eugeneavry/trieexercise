﻿using System;
using System.Collections.Generic;

namespace PrefixSearch
{
    public class PrefixTreeNode
    {
        private readonly Lazy<Dictionary<char, PrefixTreeNode>> _dictionary = new Lazy<Dictionary<char, PrefixTreeNode>>(new Dictionary<char, PrefixTreeNode>());

        public Dictionary<char, PrefixTreeNode> Children => _dictionary.Value;

        public bool IsEndOfWord { get; set; } = false;
    }
}
