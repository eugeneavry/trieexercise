# Please note

No Turkish Test conducted ;)

Various time and space complexity optimizations could be further attempted:
1) Loops may be slimmed down
2) All the Managed Memory/GC pressure could be avoided while dealing with excessive numbers of string objects by resorting to ReadOnlySpan<T> / Memory<T>

Cheers,
Eugene
