﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PrefixSearch
{
    public partial class Program
    {
        private const string OutputFileName = "output.txt";
        private const string InputFileName = "input.txt";
        private const int MaximumWordLength = 6;

        public static void Main()
        {
            PrefixTreeNode prefixTree = new PrefixTreeNode();
            HashSet<string> wholeWords = new HashSet<string>();

            PopulateDataStructures(prefixTree, wholeWords, new PrefixTreeBuilder());

            IEnumerable<string> output = ProcessAllWords(prefixTree, wholeWords, new PrefixTreeSearcher());

            File.WriteAllLines(OutputFileName, output);
        }

        private static IEnumerable<string> ProcessAllWords(PrefixTreeNode prefixTree, IEnumerable<string> words, PrefixTreeSearcher trieSearcher)
        {
            var output = Enumerable.Empty<string>();
            foreach (var word in words)
            {
                var combinations = trieSearcher.GetCharacterCombinationsForWord(prefixTree, word).Select(combination => $"{combination}={word}");
                output = output.Concat(combinations);
            }

            return output;
        }

        private static void PopulateDataStructures(PrefixTreeNode prefixTree, HashSet<string> wholeWords, PrefixTreeBuilder trieBuilder)
        {
            foreach (var line in File.ReadLines(InputFileName))
            {
                if (line.Length < MaximumWordLength)
                {
                    trieBuilder.AddWordToPrefixTree(prefixTree, line);
                }
                if (line.Length == MaximumWordLength)
                {
                    wholeWords.Add(line);
                }
            }
        }
    }
}
