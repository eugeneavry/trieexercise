﻿namespace PrefixSearch
{
    public partial class Program
    {
        public class PrefixTreeBuilder
        {
            public void AddWordToPrefixTree(PrefixTreeNode currentTreeNode, string word)
            {
                foreach (char symbol in word)
                {
                    if (!currentTreeNode.Children.ContainsKey(symbol))
                    {
                        currentTreeNode.Children.Add(symbol, new PrefixTreeNode());
                    }

                    currentTreeNode = currentTreeNode.Children[symbol];
                }

                currentTreeNode.IsEndOfWord = true;
            }
        }
    }
}
